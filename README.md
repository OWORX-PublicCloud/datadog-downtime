# dd-downtime

dd-downtime is a Python script that creates the Terraform code to schedule planned downtime in Datadog

## Requirements
* Python3

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install dd-downtime.

```bash
git clone git@bitbucket.org:OWORX-PublicCloud/datadog-downtime.git

cd datadog-downtime

pip install .
```

## Usage

Use default input
```bash
python3 dd_downtime.py
```
Specifiy input file
```bash
python3 dd_downtime.py -f filename.json
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
