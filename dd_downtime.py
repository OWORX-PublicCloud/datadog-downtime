#!/usr/bin/env python3

'''Command Line Interface.

Functions:

    :`keep_schedule`:       check id downtime schedule is current or expired
    :`_check_recurrence`:   checks key `recurrence` exists. returns True if it does
    :`_check_until_date`:   checks key `until_date` exists. returns false if is doesn't. \
                            If exists, checks date is current. returns True if it is
    :`_check_end_date`:     checks key end_date value is current. returns True if it is
'''

import argparse
import json
import datetime
import time
import jinja2


def keep_schedule(json_data):
    ''' check id downtime schedule is current or expired

    **Return Value(s)**

    Boolen
    '''
    recurrence = _check_recurrence(json_data['MaintenanceSchedule'])
    if recurrence:
        if _check_until_date(json_data['MaintenanceSchedule']['recurrence']):
            return False
        else:
            return True
    if not recurrence:
        return _check_end_date(json_data['MaintenanceSchedule'])


def _check_recurrence(json_data):
    ''' checks key `recurrence` exists. returns True if it does

    **Return Value(s)**

    Boolean
    '''
    if "recurrence" in json_data:
        return True # keep schedule
    else:
        return False # remove schedule

def _check_until_date(json_data):
    ''' checks key `until_date` exists. returns false if is doesn't.
        If exists, checks date is current. returns True if it is

    **Return Value(s)**

    Boolean
    '''
    if "until_date" in json_data:
        if time.time()>time.mktime(datetime.datetime.strptime(json_data['until_date'], \
            "%Y-%m-%dT%M:%H:%S%z").timetuple()):
            return True # remove schedule
        else:
            return False # Keep schedule
    else:
        return False # until_date does not exist. keep schedule

def _check_end_date(json_data):
    ''' checks key end_date value is current. returns True if it is

    **Return Value(s)**

    Boolean
    '''
    if time.time()<time.mktime(datetime.datetime.strptime(json_data['end_date'], \
        "%Y-%m-%dT%M:%H:%S%z").timetuple()):
        return True # keep schedule
    else:
        return False # remove schedule


def main():
    ''' reads in json configuration file and ond outputs terraform code
        for datadog downtime schedules

    **Return Value(s)**

    terraform code: downtime.tf
    '''
    # Input JSON configuration file (default:scheduledMaintenance.json)
    main_parser = argparse.ArgumentParser(description='Datadog Downtime Scheduler.')
    main_parser.add_argument(
        '--filename', '-f',
        help='JSON file input containing maintenance configuration \
              (default: scheduledMaintenance.json).',
        default="scheduledMaintenance.json"
    )
    args = main_parser.parse_args()

    # Open json file
    with open(args.filename, 'r') as all_schedules:
        data=all_schedules.read()

    maintenances = json.loads(data)

    # Build schedules dictionary from json
    schedules = []

    for maintenance in maintenances['ScheduledMaintenances']:
        if keep_schedule(maintenance):
            schedule = {}
            schedule.update({
                'maintenanceId': maintenance['MaintenanceId'],
                'scope': str(maintenance['Scope']).replace("'",'"'),
                'monitorId': maintenance['MonitorId'],
                'requestId': maintenance['RequestId'],
                'startDate': maintenance['MaintenanceSchedule']['start_date'],
                'endDate': maintenance['MaintenanceSchedule']['end_date'],
            })
            if 'recurrence' in maintenance['MaintenanceSchedule']:
                schedule.update({
                    'type': maintenance['MaintenanceSchedule']['recurrence']['type'],
                    'period': maintenance['MaintenanceSchedule']['recurrence']['period'],
                    'weekDays': str(maintenance['MaintenanceSchedule']['recurrence']['week_days']).replace("'",'"') if 'recurrence' in maintenance['MaintenanceSchedule'] else "null",
                    'untilOccurrences': maintenance['MaintenanceSchedule']['recurrence']['until_occurrences'] if 'until_occurrences' in maintenance['MaintenanceSchedule']['recurrence'] else "null",
                    'untilDate': time.mktime(datetime.datetime.strptime(maintenance['MaintenanceSchedule']['recurrence']['until_date'], "%Y-%m-%dT%M:%H:%S%z").timetuple()) if 'until_date' in maintenance['MaintenanceSchedule']['recurrence'] else "null"
                })
            schedules.append(schedule)
        else:
            print("Maintenance ID:" + maintenance['MaintenanceId'] + " Expired and will be removed")


    # Render jinja template using schedules dictionary
    output = (jinja2.Environment(
        loader=jinja2.FileSystemLoader("./templates/")
        ).get_template("dd_downtime.j2").render(schedules = schedules))

    # Output Terraform to file
    with open('downtime.tf', 'w') as filename:
        filename.write(output)


if __name__ == '__main__':
    main()