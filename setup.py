#!/usr/bin/env python3

import setuptools

with open('requirements.txt', 'r') as f:
    requirements = [i.strip() for i in list(f)]

setuptools.setup(
  name='dd-downtime',
  version='1.0.0',
  packages=setuptools.find_packages('.', exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
  install_requires=requirements,
)
